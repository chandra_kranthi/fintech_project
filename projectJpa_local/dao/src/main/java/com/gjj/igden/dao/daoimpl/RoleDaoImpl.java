package com.gjj.igden.dao.daoimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.gjj.igden.dao.AbstractDAO;
import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.model.Role;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class RoleDaoImpl extends AbstractDAO<Role> {

	@Override
	public Role read(Role obj) {
		return (Role) em.createQuery("from Role where id = "+obj.getId()).getSingleResult();
	}

	
	@Override
	public List<Role> readAll() {
		return em.createQuery("from Role").getResultList();
	}
	
	public Long getCount() {
		Long max = (Long) em.createQuery("select COUNT(*) FROM Role").getSingleResult();
		return null == max ? 0 : max;
	}
	
	public void createDefaultRole() throws DAOException {
			Role role = new Role();
			role.setName("ADMIN");
			super.create(role);
	}
	
}